package com.example;

import java.util.Scanner;

public class NumberExample {
    private int firstNumber;
    Scanner scanner=new Scanner(System.in);
    public void input(){
        System.out.println("Enter number");
        firstNumber =scanner.nextInt();
    }
    public void checkNumber() throws MyException {
        if(firstNumber >100){
            throw new MyException("Number is greater then 100");
        }else{
            System.out.println(firstNumber);
        }
    }

}

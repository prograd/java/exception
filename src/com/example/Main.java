package com.example;

public class Main {
    public static void main(String[] args) {


        try {
            NumberExample numberExample = new NumberExample();
            numberExample.input();
            numberExample.checkNumber();

        } catch (MyException e) {
            System.out.println(e.getMessage());
        }
    }
}
